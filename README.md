Ecommerce-service
==============

<!--[![pipeline status](https://gitlab.com/maxwellkenned/ecommerce-service/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/maxwellkenned/ecommerce-service/commits/master)-->
<!--[![coverage report](https://gitlab.com/maxwellkenned/ecommerce-service/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/maxwellkenned/ecommerce-service/commits/master)-->
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/maxwellkenned/ecommerce-service?style=flat-square)](https://gitlab.com/maxwellkenned/ecommerce-service/commits/master)
[![Codecov](https://img.shields.io/codecov/c/gitlab/maxwellkenned/ecommerce-service?style=flat-square)](https://gitlab.com/maxwellkenned/ecommerce-service/commits/master)

Back end project made in symfony 4 for the largest ecommerce in the world, perhaps Brazil.

# Installation

First, create a folder from the computer root:

```bash
sudo mkdir /projetos && cd /projetos
```

Then clone this repository:
 
```bash
git clone https://gitlab.com/maxwellkenned/ecommerce-service.git
```

Then enter the cloned directory:

```bash
cd /ecommerce-service
```

Then run the following command to run the project:
```bash
composer dev
```

Finish, you can visit an app by the following URL:

#### http://localhost/api/


# Use xdebug!

### PhpStorm

> In: File > Settings > Languages & Frameworks > PHP > Servers
>
>Create a new server, in the Port field put the nginx external port:

![Server Creating](https://i.ibb.co/bN2bM7S/1.png)
>
> Then on: Run > Edit Configurations...
>
> Create a new PHP Remote Debug:

![Creating remote debug](https://i.ibb.co/YczcyLT/2.png)
