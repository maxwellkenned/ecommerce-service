<?php

namespace App\Controller\Auth;

use FOS\RestBundle\Controller\Annotations as Rest;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GoogleController
 *
 * @package App\Controller\Auth
 */
class GoogleController extends AbstractController
{
    /**
     * Link to this controller to start the "connect" process
     *
     * @Rest\Get("/auth/google", name="connect_google")
     * @param ClientRegistry $clientRegistry
     *
     * @return RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('google')
            ->redirect(['profile', 'email']);
    }

    /**
     * Google redirects to back here afterwards
     *
     * @codeCoverageIgnore
     * @Rest\Get("/auth/google/check", name="connect_google_check")
     * @param Request        $request
     * @param ClientRegistry $clientRegistry
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
    }
}
