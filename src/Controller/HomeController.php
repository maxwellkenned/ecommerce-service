<?php

namespace App\Controller;

use FOS\RestBundle\Controller\{
    AbstractFOSRestController,
    Annotations as Rest
};
use App\Entity\User;
use Exception;
use Symfony\Component\HttpFoundation\{JsonResponse, Request, Response};
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\Provider\GoogleClient;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class HomeController
 *
 * @package App\Controller
 */
class HomeController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/", name="index")
     * @param SerializerInterface $serializer
     *
     * @return string
     */
    public function indexAction(
        SerializerInterface $serializer
    ) {
        return new Response($serializer->serialize($this->getUser(), 'json'));
    }

    /**
     * @TODO: Migrar para classe AuthController
     * @Rest\Get("/logout", name="app_logout")
     * @codeCoverageIgnore
     * @throws Exception
     */
    public function logoutAction()
    {
        throw new Exception('Don\'t forget to activate logout in security.yaml');
    }

    /**
     * @TODO: Migrar para classe AuthController
     *
     * @Rest\Get("/login", name="app_login")
     * @codeCoverageIgnore
     * @throws Exception
     */
    public function loginAction()
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('connect_google');
        }

        return $this->redirectToRoute('index');
    }
}
