<?php

namespace App\Tests\ControllerTest\AuthTest;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class GoogleControllerTest
 *
 * @package App\Tests\ControllerTest\AuthTest
 */
class GoogleControllerTest extends WebTestCase
{
    /** @var KernelBrowser $client */
    private $client = null;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @dataProvider provideUrls
     *
     * @param $url
     */
    public function testPageIsSucessful($url)
    {
        $this->client->request('GET', $url);

        $testReturn = $this->client->getResponse()->isSuccessful() || $this->client->getResponse()->isRedirect();

        $this->assertTrue($testReturn);
    }

    /**
     * @dataProvider provideUrls
     *
     * @param $url
     */
    public function testPageIsNotSucessfull($url)
    {
        $this->client->request('GET', $url);

        $testReturn = $this->client->getResponse()->isClientError() || $this->client->getResponse()->isServerError();

        $this->assertFalse($testReturn);
    }

    public function provideUrls()
    {
        return [
            'connectAction' => ['/api/auth/google'],
            'connectCheckAction' => ['/api/auth/google/check'],
        ];
    }
}
