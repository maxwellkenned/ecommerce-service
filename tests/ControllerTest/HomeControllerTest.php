<?php

namespace App\Tests\ControllerTest;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HomeControllerTest
 *
 * @package App\Tests\ControllerTest
 */
class HomeControllerTest extends WebTestCase
{
    /** @var KernelBrowser $client */
    private $client = null;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @dataProvider provideUrls
     *
     * @param $url
     */
    public function testPageIsSucessful($url)
    {
        $this->client->request('GET', $url);

        $testReturn = $this->client->getResponse()->isSuccessful() || $this->client->getResponse()->isRedirect();

        $this->assertTrue($testReturn);
    }

    /**
     * @dataProvider provideUrls
     *
     * @param $url
     */
    public function testPageIsNotSucessfull($url)
    {
        $this->client->request('GET', $url);

        $testReturn = $this->client->getResponse()->isClientError() || $this->client->getResponse()->isServerError();

        $this->assertFalse($testReturn);
    }

    public function provideUrls()
    {
        return [
            'indexAction' => ['/api/'],
            'loginAction' => ['/api/login/'],
            'logoutAction' => ['/api/logout/'],
        ];
    }
}
