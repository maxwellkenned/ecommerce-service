<?php

namespace App\Tests\RepositoryTest;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductRepositoryTest extends KernelTestCase
{
    /** @var ProductRepository */
    private $productRepository;

    /** @var EntityManager */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->productRepository = $this->entityManager
            ->getRepository(Product::class);
    }

    /**
     * @dataProvider productProviders
     *
     * @param string $name
     * @param float $salePrice
     * @param float $purchasePrice
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testSave(string $name, float $salePrice, float $purchasePrice)
    {
        $product = new Product();
        $product->setName($name);
        $product->setSalePrice($salePrice);
        $product->setPurchasePrice($purchasePrice);

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        $this->assertTrue(!is_null($product->getId()));
    }

    /**
     * @return array
     */
    public function productProviders(): array
    {
        return [
            "product 1" => ["Foo Bar", 10.00, 10.00],
            "product 2" => ["Bar Foo", 20.05, 20.05],
        ];
    }

    /**
     * @depends      testSave
     * @dataProvider productProviders
     *
     * @param string $name
     */
    public function testFindOneBy(string $name)
    {
        /** @var Product $product */
        $product = $this->productRepository->findOneBy(['name' => $name]);

        $this->assertEquals($product->getName(), $name);
    }

    /**
     * @depends      testSave
     * @dataProvider productProviders
     *
     * @param string $name
     */
    public function testFindBy(string $name)
    {
        /** @var Product $product */
        $product = current($this->productRepository->findBy(['name' => $name]));

        $this->assertEquals($product->getName(), $name);
    }


    /**
     * @depends      testSave
     * @dataProvider productProviders
     */
    public function testFindAll()
    {
        $product = $this->productRepository->findAll();

        $countProductProvider = count($this->productProviders());

        $this->assertEquals($countProductProvider, count($product));
    }


    /**
     * @depends      testFindOneBy
     * @dataProvider productProviders
     *
     * @param string $name
     */
    public function testFind(string $name)
    {
        $productProvider = $this->productRepository->findOneBy(['name' => $name]);

        $product = $this->productRepository->find($productProvider->getId());

        $this->assertEquals($product->getName(), $productProvider->getName());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
