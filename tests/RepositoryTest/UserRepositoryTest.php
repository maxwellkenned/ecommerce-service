<?php

namespace App\Tests\RepositoryTest;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase
{
    /** @var User $user */
    private $user;

    /** @var UserRepository */
    private $userRepository;

    /** @var EntityManager */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->userRepository = $this->entityManager
            ->getRepository(User::class);
    }

    /**
     * @dataProvider providerUsers
     *
     * @param string $name
     * @param string $email
     * @param string $password
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testSave(string $name, string $email, string $password)
    {
        $user = new User();
        $user->setFullname($name);
        $user->setEmail($email);
        $user->setPassword(
            self::$container->get('security.password_encoder')->encodePassword($user, $password)
        );

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->assertTrue(!is_null($user->getId()));
    }

    /**
     * @depends      testSave
     * @dataProvider providerUsers
     *
     * @param string $name
     * @param string $email
     */
    public function testFindOneBy(string $name, string $email)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['email' => $email]);

        $this->assertEquals($user->getEmail(), $email);
    }

    /**
     * @depends      testSave
     * @dataProvider providerUsers
     *
     * @param string $name
     * @param string $email
     */
    public function testFindBy(string $name, string $email)
    {
        /** @var User $user */
        $user = current($this->userRepository->findBy(['email' => $email]));

        $this->assertEquals($user->getEmail(), $email);
    }


    public function testFindAll()
    {
        $users = $this->userRepository->findAll();

        $countUsersProvider = count($this->providerUsers());

        $this->assertEquals($countUsersProvider, count($users));
    }


    /**
     * @depends      testFindOneBy
     * @dataProvider providerUsers
     *
     * @param string $name
     * @param string $email
     */
    public function testFind(string $name, string $email)
    {
        $userProvider = $this->userRepository->findOneBy(['email' => $email]);

        $user = $this->userRepository->find($userProvider->getId());

        $this->assertEquals($user->getEmail(), $userProvider->getEmail());
    }

    /**
     * @return array
     */
    public function providerUsers(): array
    {
        return [
            "usuário 1" => ["Foo Bar", 'foo@bar.com.br', '111'],
            "usuário 2" => ["Bar Foo", 'bar@foo.com.br', '222'],
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
